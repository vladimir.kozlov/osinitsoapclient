package com.osinitsoap;

import javax.xml.soap.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class SoapClient {
    public static void main(String[] args) {
        System.out.print("Please, enter user ID: ");

        String userId = null;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            userId = reader.readLine();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        if (userId == null || userId.trim().equals("")) {
            System.out.println("Wrong user ID!");
            return;
        }

        callSoapWebService("http://localhost:8181/cxf/users/welcome", "http://osinitsoap.com", userId);
    }

    private static void createSoapEnvelope(SOAPMessage soapMessage, String userId) throws SOAPException {
        SOAPPart soapPart = soapMessage.getSOAPPart();

        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration("osin", "http://osinitsoap.com");

        // SOAP Body
        SOAPBody soapBody = envelope.getBody();
        SOAPElement soapBodyElem = soapBody.addChildElement("getUserRequest", "osin");
        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("inputUserId");
        soapBodyElem1.addTextNode(userId);
    }

    private static void callSoapWebService(String soapEndpointUrl, String soapAction, String userId) {
        try {
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            // Send SOAP Message to SOAP Server
            SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(soapAction, userId), soapEndpointUrl);

            // Print the SOAP Response
            System.out.println("Response SOAP Message:");
            soapResponse.writeTo(System.out);
            System.out.println();

            soapConnection.close();
        } catch (Exception e) {
            System.err.println("\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
            e.printStackTrace();
        }
    }

    private static SOAPMessage createSOAPRequest(String soapAction, String userId) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();

        createSoapEnvelope(soapMessage, userId);

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", soapAction);

        soapMessage.saveChanges();

        /* Print the request message, just for debugging purposes */
        System.out.println("Request SOAP Message:");
        soapMessage.writeTo(System.out);
        System.out.println("\n");

        return soapMessage;
    }

}
